public class Point {
    float x1 ,x2;

    public Point(float x1, float x2) {
        this.x1 = x1;
        this.x2 = x2;
    }

    public Point() {
    }


    public String toString(){
        return"x1: "+x1+"     x2: "+x2 ;
    }
}
