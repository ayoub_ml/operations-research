public class Systeme {
    Equation e1 , e2;
    Point p ;

    public Systeme(Equation e1, Equation e2) {
        this.e1 = e1;
        this.e2 = e2;
        p = new Point();
    }

//    public boolean testParal(Equation e1 , Equation e2){
//        return (e1.a/e2.a == e1.b/e2.b);
//    }

    public void simplification () {
        if(e1.a == -e2.a){
            p.x2 = ( e1.c + e2.c ) / (e1.b + e2.b);
        }else if(e1.b == -e2.b){
            p.x1 = ( e1.c + e2.c ) / (e1.a + e2.a);
        }
    }

    public void resultatX1(){
        e1.x2EnfonctionX1();
        p.x1 =  (e2.c-e2.b*e1.c1) / (e2.a+e2.b*e1.a1);
    }

    public void resultatX1X2(){
        resultatX1();
        System.out.println("");
//        x2 = (-e2.b*x1 + e2.c)/e2.b ;
        p.x2 = (-e2.a*p.x1 + e2.c)/e2.b ;
    }

}
